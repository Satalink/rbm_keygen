<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Recycle Bin Manager : Activation</title>
    </head>
<style>
body {
	margin: 0;
	padding: 0;
	background: #3D3D3D url(../images/body-bg.png);
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #DDDDDD;
}
</style>
    <body>
        <!-- Google Code for RBM Conversion 1 Conversion Page -->
        <noscript>
        <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1031589075/?value=19.95&amp;label=b2NnCMfmxQEQ05nz6wM&amp;guid=ON&amp;script=0"/>
        </div>
        </noscript>
        <script type="text/javascript">
        <!--
        var google_conversion_id = 1031589075;
        var google_conversion_language = "en";
        var google_conversion_format = "1";
        var google_conversion_color = "333333";
        var google_conversion_label = "b2NnCMfmxQEQ05nz6wM";
        var google_conversion_value = 0;
        if (19.95) {
          google_conversion_value = 19.07;
        }
        //-->
        </script>
        <script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
        </script>
        <?php
             $error = false;
             $email = isset($_POST['email']) ? trim($_POST['email']) : '';
             if(!$email){
                 $error=true;  //First time visit..
             } else {
                //Email invalid and not empty
                 if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email)) {
                     echo '<center><font color="red">Valid Email Address</font> is required.  Your registration key will be sent to this email address.</center><BR>';
                     $error=true;
                 }
             }

             if ($error) {
                echo '<center>';
                echo '<form action="RBM_Purchase.php" method="POST">';
 		echo '<table border="0" width="275">';
                echo '<tr><td> </td><td align="center"><h2><font color="#33FF33"><b>Product Registration</font></h2></td></tr>';
                echo '<tr><td align="right" width="20%">Email:</td><td width="67%"><input type="text" name="email" size="25" value="'.$email.'"></td></tr>';
		echo '<tr><td width="50%"> </td><td width="50%" align="right"><input type="submit" name="SubmitForm" value="Submit"></td></tr>';
                echo '</table>';
                echo '</form>';
                echo '</center>';
             } else {
                Activation($email);
                sleep(3);
                echo '<script language=JavaScript>window.close();</script>';
                exit;
             }

            // Functions

            function getUserKey($emailaddr,$keys){
                $emailaddr = strtoupper($emailaddr);
                $emailChars = str_split($emailaddr);
                $keyArray = explode('|', $keys);
                $regKey = "";
                for($c=0;$c<sizeof($emailChars);$c++){
                    $pos = rand(0,strlen($keyArrays[$c]));
                    $regKey .= substr($keyArray[$c], $pos, 1);
                }
                return($regKey);
            }
            function getUserKeys($emailaddr,$keyCrypt){
                $key="";
                $emailaddr = strtoupper($emailaddr);
                $keyArray = explode(',', $keyCrypt);
                $emailChars = str_split($emailaddr);
                for($b=0;$b<sizeof($emailChars);$b++){
                    $x=ord($emailChars[$b]);
                    if($x>47 && $x<58 && $keyArray>$x){
                        $key .= $keyArray[$x].'|';
                    } else if($x>64 && $x<91 && $keyArray>$x){
                        $key .= $keyArray[$x].'|';
                    }
                } 
                return($key);
            }
            function CreateRndCode(){
                $size=128;
                $returncode="";
                for($i=0;$i<$size;$i++){
                    $chrString="";
                    for($l=0;$l<=5;$l++){
                        $f=rand(0, 20);
                        if($f < 5){
                            //Digit
                            $c=chr(rand(48,57));
                        } else {
                            //Alpha
                            $c=chr(rand(65,90));
                        }
                            $chrString .= $c;
                    }
//                    $returncode .= $chrString;
                    if($i<$size-1){
                        $returncode .= $chrString.",";
                    } else {
                        $returncode .= $chrString;
                    }
                }
                return($returncode);
            }
            function Activation($emailaddr){
                $server = "localhost";
                $dbName = get_current_user()."_RBM"; 
                $user = get_current_user()."_RBMadm";
                $pass = "B3S5FA3D4DS";
                $connected = mysql_connect($server, $user, $pass);
                if($connected){
                    $keycrypt = CreateRndCode();
                    $keys = getUserKeys($emailaddr, $keycrypt);
                    $regKey = getUserKey($emailaddr, $keys);
                        mysql_select_db(get_current_user()."_RBM");
                        $result = mysql_query("INSERT INTO `USERS` (NAME, STATE, KEYCRYPT, TIMESTAMP) VALUES ('$emailaddr','AVAILABLE','$keycrypt',CURRENT_TIMESTAMP)");
                        mysql_select_db(get_current_user()."_DBLog");
                        $result = mysql_query("INSERT INTO `LOG` (ENTRY, USER, TIMESTAMP) VALUES ('ACTIVATED','$emailaddr',CURRENT_TIMESTAMP)");
                        echo '<BR><BR><CENTER>'.$emailaddr.' is now REGISTERED!<BR>You will recieve your activation key by email shortly.  Thank you!</CENTER>';
                        $headers = 'From: billing@satalinksoft.com' . "\r\n" .'Reply-To: support@satalinksoft.com' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
                        mail($emailaddr, "Recycle Bin Manager Activation Key", "Thank you for your purchase of Recycle Bin Manager.  You'll find your activation user name and key below.  The activation process requires internet access.  Please ensure that you are connected to the internet and that you allow the Recycle Bin Manager process access in your firewall settings for the duration of the activation process.\n\n\n\tRegistered User Name: $emailaddr\n\tActivation Key: $regKey\n\n\nIf you have any problems, please visit our support center at http://support.satalinksoft.com\n\n Thank you!\nSatalinkSoft.com", $headers);
                        mail("support@satalinksoft.com","Recycle Bin Manager Registered","Recycle Bin Manager has been successfully registered to $emailaddr", $headers);
                    mysql_close();

                }
            }
        ?>
    </body>
</html>
